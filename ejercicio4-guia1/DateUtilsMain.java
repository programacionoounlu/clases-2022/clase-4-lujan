package ejerciciosAlumnos.ejercicio4;

import java.time.LocalDate;

public class DateUtilsMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LocalDate fecha1 = DateUtils.getFechaDesdeFormato("22-11-2022", DateUtils.FORMATO1);
		LocalDate fecha2 = DateUtils.getFechaDesdeFormato("10-01-2022", DateUtils.FORMATO2);
		System.out.println("La fecha 1 es mayor a la fecha 2: "+DateUtils.esMayor(fecha1, fecha2));
		System.out.println("La fecha 2 es mayor a la fecha 1: "+DateUtils.esMayor(fecha2, fecha1));
		
		LocalDate fechaLimiteInferior = DateUtils.getFechaDesdeFormato("01-02-2022", DateUtils.FORMATO1);
		LocalDate fechaLimiteSuperior = DateUtils.getFechaDesdeFormato("01-05-2022", DateUtils.FORMATO1);
		LocalDate fechaAConsultar = DateUtils.getFechaDesdeFormato("01-03-2022", DateUtils.FORMATO1);
		
		System.out.println("La fecha a consultar se encuentra entre el limite inferior y superior: "+DateUtils.estaEntreFechas(fechaAConsultar, fechaLimiteInferior, fechaLimiteSuperior));
	}

}

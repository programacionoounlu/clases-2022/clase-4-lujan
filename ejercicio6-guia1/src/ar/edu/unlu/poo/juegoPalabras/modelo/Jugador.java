package ar.edu.unlu.poo.juegoPalabras.modelo;

import java.util.ArrayList;

public class Jugador {
	private ArrayList<Palabra> palabras;
	private static int jugador_siguiente = 1;
	private String nombre;
	
	public Jugador(String nombre) {
		Jugador.jugador_siguiente += 1;
		this.nombre = nombre;
		this.palabras = new ArrayList<Palabra>();
	}
	
	public Jugador() {
		this("Player"+String.valueOf(jugador_siguiente));
	}
	
	public Jugador(int cantidadJugadoresDelJuego) {
		this("Player"+String.valueOf(cantidadJugadoresDelJuego+1));
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public void addPalabra(Palabra p) {
		this.palabras.add(p);
	}
	
	public int getPuntaje() {
		int res = 0;
		for(Palabra p: this.palabras) {
			res += p.getPuntaje();
		}
		return res;
	}
	

}

package ar.edu.unlu.poo.juegoPalabras.main;

import ar.edu.unlu.poo.juegoPalabras.modelo.Jugador;
import ar.edu.unlu.poo.juegoPalabras.modelo.Palabra;

public class Main {

	public static void main(String[] args) {
		Palabra p = new Palabra("comwputadorw");
		System.out.println(p.getPuntaje());
		
		Jugador j = new Jugador(10);
		Jugador j2 = new Jugador("Juan");
		Jugador j3 = new Jugador();
		
		System.out.println(j.getNombre());
		System.out.println(j2.getNombre());
		System.out.println(j3.getNombre());
		
		j.addPalabra(p);

	}

}

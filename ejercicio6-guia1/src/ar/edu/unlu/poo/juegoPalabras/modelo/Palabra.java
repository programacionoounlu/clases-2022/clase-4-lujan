package ar.edu.unlu.poo.juegoPalabras.modelo;

import java.util.ArrayList;

public class Palabra {
	
	private static final char[] CARACTERES_ESPECIALES = {'k','z','x','y','w','q'};
	private String palabra;
	
	
	public Palabra(String palabra) {
		this.palabra = palabra;
	}
	
	public String getPalabra() {
		return this.palabra;
	}
	
	public int getPuntaje() {
		int res = 0;
		
		for(char c: this.palabra.toCharArray()) {
			if(this.esCaracterEspecial(c)) {
				res += 2;
			}else {
				res += 1;
			}
		}
		
		return res;
	}
	
	private boolean esCaracterEspecial(char c) {
		for(char cc: Palabra.CARACTERES_ESPECIALES) {
			if(cc == c) {
				return true;
			}
		}
		
		return false;
	}
}
